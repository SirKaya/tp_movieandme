import React from 'react'
import { StyleSheet, View, Image, Text } from 'react-native'
import Search from "./Search";

class Test extends React.Component {
    render() {
        return (
            <View style ={{flex: 1, backgroundColor: 'yellow', justifyContent: 'flex-end', alignItems: 'center'}}>
                <View style ={{height: 50, width: 50, backgroundColor: 'red'}}></View>
                <View style ={{height: 50, width: 50, backgroundColor: 'green'}}></View>
                <View style ={{height: 50, width: 50, backgroundColor: 'blue'}}></View>
            </View>
        )
    }
}

export default Test
